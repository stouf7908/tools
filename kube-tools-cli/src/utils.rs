/// Quick'n'dirty error reporting!
pub type BoxError = Box<dyn std::error::Error + Sync + Send>;
/// Quick'n'dirty error reporting!
pub type BoxResult<T> = Result<T, BoxError>;

/// The quick'n'dirty logging framework!
macro_rules! log {
    ($($tt:tt)*) => {
        eprintln!($($tt)*)
    }
}
