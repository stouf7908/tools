use std::collections::HashMap;

use futures::StreamExt;
use image::{Rgb, RgbImage};

use crate::crawler::{ChunkStatus, ChunkStatusMap};
use crate::db::KubeDb;
use crate::region::ZoneBounds;
use crate::utils::BoxResult;

const COLOR_EMPTY: Rgb<u8> = Rgb([0xFF, 0xFF, 0xFF]);
const COLOR_GENERATED: Rgb<u8> = Rgb([0x70, 0x70, 0xFF]);
const COLOR_INHABITED: Rgb<u8> = Rgb([0x00, 0xFF, 0x00]);
const COLOR_UNGENERATED: Rgb<u8> = Rgb([0x22, 0x22, 0x22]);
const COLOR_POTENTIAL: Rgb<u8> = Rgb([0xFF, 0x00, 0x00]);
const CHUNK_SIZE: i32 = kube_core::region::ZONES_PER_CHUNK as i32;

fn color_partially_inhabited(n: u8) -> Rgb<u8> {
    let n = n.min(64);
    Rgb([0x80 - n * 2, 0xFF, 0xC0 - n * 3])
}

pub async fn render_image(db: &KubeDb, bounds: ZoneBounds) -> BoxResult<RgbImage> {
    let bounds = bounds.dezoom(CHUNK_SIZE as u32);
    let mut image = empty_image(bounds);

    let mut fill_chunk = |cx: i32, cy: i32, color: Rgb<u8>| {
        let (x, y) = to_screen_space(bounds, cx, cy).expect("out of bounds");
        image.put_pixel(x, y, color);
    };

    render_chunk_statuses(db, &mut fill_chunk).await?;
    render_inhabited_zones(db, &mut image, bounds).await?;

    Ok(image)
}

pub async fn render_image_zoomed(db: &KubeDb, bounds: ZoneBounds) -> BoxResult<RgbImage> {
    let mut image = empty_image(bounds);

    let mut fill_chunk = |cx: i32, cy: i32, color: Rgb<u8>| {
        for j in 0..CHUNK_SIZE {
            for i in 0..CHUNK_SIZE {
                let pos = to_screen_space(bounds, cx * CHUNK_SIZE + i, cy * CHUNK_SIZE + j);
                if let Some((x, y)) = pos {
                    image.put_pixel(x, y, color);
                }
            }
        }
    };

    render_chunk_statuses(db, &mut fill_chunk).await?;
    render_inhabited_zones_zoomed(db, &mut image, bounds).await?;

    Ok(image)
}

async fn render_chunk_statuses(
    db: &KubeDb,
    render_chunk: &mut dyn FnMut(i32, i32, Rgb<u8>),
) -> BoxResult<()> {
    let mut chunks = db.get_chunks_status();
    let mut map = ChunkStatusMap::new((0, 0));
    while let Some(chunk) = chunks.next().await {
        let chunk = chunk?;
        let (color, status) = if chunk.generated {
            (COLOR_GENERATED, ChunkStatus::Generated)
        } else {
            (COLOR_UNGENERATED, ChunkStatus::Ungenerated)
        };
        map.set_status((chunk.cx, chunk.cy), status);
        render_chunk(chunk.cx, chunk.cy, color);
    }

    for (cx, cy) in map.missing_chunks() {
        render_chunk(cx, cy, COLOR_POTENTIAL);
    }

    Ok(())
}

async fn render_inhabited_zones(
    db: &KubeDb,
    image: &mut RgbImage,
    bounds: ZoneBounds,
) -> BoxResult<()> {
    // Gather inhabited zones by chunk.
    let mut inhabited_zones = db.get_inhabited_zones();
    let mut map = HashMap::new();
    while let Some(zone) = inhabited_zones.next().await {
        let (x, y) = zone?;
        if let Some(pos) = to_screen_space(bounds, x / CHUNK_SIZE, y / CHUNK_SIZE) {
            *map.entry(pos).or_insert(0u8) += 1;
        }
    }

    // Choose the pixel color depending of the number of inhabited zones.
    for ((x, y), qty) in map {
        image.put_pixel(x, y, color_partially_inhabited(qty));
    }

    Ok(())
}

async fn render_inhabited_zones_zoomed(
    db: &KubeDb,
    image: &mut RgbImage,
    bounds: ZoneBounds,
) -> BoxResult<()> {
    let mut inhabited_zones = db.get_inhabited_zones();
    while let Some(zone) = inhabited_zones.next().await {
        let (x, y) = zone?;
        if let Some((x, y)) = to_screen_space(bounds, x, y) {
            image.put_pixel(x, y, COLOR_INHABITED);
        }
    }
    Ok(())
}

fn empty_image(bounds: ZoneBounds) -> RgbImage {
    let mut image = RgbImage::new(bounds.width(), bounds.height());
    for p in image.pixels_mut() {
        *p = COLOR_EMPTY;
    }
    image
}

fn to_screen_space(bounds: ZoneBounds, x: i32, y: i32) -> Option<(u32, u32)> {
    if x >= bounds.xmin() && x < bounds.xmax() && y >= bounds.ymin() && y < bounds.ymax() {
        Some(((x - bounds.xmin()) as u32, (bounds.ymax() - y - 1) as u32))
    } else {
        None
    }
}
