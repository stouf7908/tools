use image::{GenericImage, Rgb, RgbImage};

use kube_core::region;

use crate::config::CliMinimap;
use crate::db::{DbChunk, KubeDb};
use crate::utils::BoxResult;

const MAX_IMAGE_SIZE: u32 = 65536;

const COLOR_EMPTY: Rgb<u8> = Rgb([0xFF, 0xFF, 0xFF]);
const COLOR_UNGENERATED: Rgb<u8> = Rgb([0x80, 0x80, 0x80]);

const ZONES_PER_CHUNK: u32 = region::ZONES_PER_CHUNK as u32;
const BLOCKS_PER_ZONE: u32 = region::ZONE_WIDTH as u32;

fn fill_image<I: GenericImage>(img: &mut I, pixel: I::Pixel) {
    for y in 0..img.height() {
        for x in 0..img.width() {
            img.put_pixel(x, y, pixel);
        }
    }
}

pub async fn render_image(db: &KubeDb, opts: &CliMinimap) -> BoxResult<RgbImage> {
    let img_width = opts.bounds.width().saturating_mul(BLOCKS_PER_ZONE);
    let img_height = opts.bounds.height().saturating_mul(BLOCKS_PER_ZONE);

    if img_width > MAX_IMAGE_SIZE || img_height > MAX_IMAGE_SIZE {
        return Err("Image dimensions too big".into());
    }

    let mut image = RgbImage::new(img_width, img_height);
    let mut minimap = kube_core::minimap::HeightMap::new();
    let orientation = opts.orientation;

    let chunk_bounds = opts.bounds.dezoom(ZONES_PER_CHUNK as u32);

    for cy in chunk_bounds.ymin()..chunk_bounds.ymax() {
        for cx in chunk_bounds.xmin()..chunk_bounds.xmax() {
            let (img_x, off_x) = get_corner_and_offset(cx, opts.bounds.xmin());
            let (img_y, off_y) = get_corner_and_offset(cy, opts.bounds.ymin());
            let mut sub = image.sub_image(img_x, img_y, img_width - img_x, img_height - img_y);

            match db.get_chunk((cx, cy), opts.date).await? {
                Some(DbChunk {
                    data: Some(chunk), ..
                }) => {
                    let chunk = chunk.into_blocks();
                    minimap.fill_from(&chunk);
                    minimap.render(&mut *sub, off_x, off_y, orientation);
                }
                Some(_) => fill_image(&mut *sub, COLOR_UNGENERATED),
                None => fill_image(&mut *sub, COLOR_EMPTY),
            }
        }
    }

    Ok(image)
}

fn get_corner_and_offset(chunk: i32, zone_min: i32) -> (u32, u8) {
    let corner = chunk.checked_mul(ZONES_PER_CHUNK as i32).expect("overflow");
    if corner < zone_min {
        let off = (zone_min - corner) as u32;
        if off >= ZONES_PER_CHUNK {
            panic!("out-of-bounds chunk");
        }
        (0, (off * BLOCKS_PER_ZONE) as u8)
    } else {
        let pos = ((corner - zone_min) as u32)
            .checked_mul(BLOCKS_PER_ZONE)
            .expect("overflow");
        (pos, 0)
    }
}
