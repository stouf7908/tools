#[macro_use]
mod utils;

mod config;
mod crawler;
mod forums;
mod kube;
mod minimap;
mod overview;
mod region;
mod session;

use futures::prelude::*;
use kube_core::region::RegionStorage;
use tokio::sync::Mutex;

use kube_db::{self as db, KubeDb};
use twin_network::session::TwinSessionId;

use kube::{KubeHtml, KubeHtmlOptions, KubeReceiver, KubeSender};
use utils::BoxResult;

use crate::region::ZoneBounds;

async fn load_config_file(path: &std::path::Path) -> BoxResult<config::Config> {
    use tokio::io::AsyncReadExt;

    let mut string = String::new();
    tokio::fs::File::open(path).await?.read_to_string(&mut string).await?;
    Ok(toml::from_str(&string)?)
}

#[tokio::main]
async fn main() -> () {
    use config::CliCommand;

    let opts: config::CliOptions = clap::Parser::parse();
    let config = match load_config_file(&opts.config_file).await {
        Ok(config) => config,
        Err(err) => {
            eprintln!(
                "[FATAL ERROR] Couldn't open config file {}: {}",
                opts.config_file.display(),
                err
            );
            std::process::exit(1);
        }
    };

    let res = match opts.command {
        CliCommand::UpgradeDb(opts) => upgrade_database(config, opts).await,
        CliCommand::Crawl(opts) => run_crawler(config, opts).await,
        CliCommand::Overview(opts) => render_overview(config, opts).await,
        CliCommand::Minimap(opts) => render_minimap(config, opts).await,
        CliCommand::ExportKub3dit(opts) => export_kub3dit(config, opts).await,
        CliCommand::ExportRaw(opts) => export_raw(config, opts).await,
        CliCommand::ListForums(opts) => list_forums(config, opts).await,
        CliCommand::EnableForum(opts) => enable_forums(config, opts).await,
        CliCommand::Teleport(opts) => teleport(config, opts).await,
    };

    if let Err(err) = res {
        eprintln!("[FATAL ERROR] {}", err);
        std::process::exit(1);
    }
}

async fn upgrade_database(config: config::Config, opts: config::CliUpgradeDb) -> BoxResult<()> {
    let db = db::Config::from(config.database)
        .upgrade(opts.dry_run)
        .await?;

    if opts.compress_chunks {
        upgrade_db_compress_chunks(&db, opts.parallel_jobs).await?;
    }

    db.close().await;
    Ok(())
}

async fn upgrade_db_compress_chunks(db: &KubeDb, parallel_jobs: usize) -> BoxResult<()> {
    use kube_core::region::ChunkPos;

    let chunks: Vec<ChunkPos> = db.get_uncompressed_chunks().try_collect().await?;
    let nb_chunks = chunks.len();

    log!("Compressing {} chunks...", nb_chunks);

    let mut jobs = stream::iter(chunks)
        .map(|chunk_pos| async move { (chunk_pos, db.compress_chunk(chunk_pos).await) })
        .buffer_unordered(parallel_jobs);

    let mut compressed = 0usize;
    while let Some((chunk_pos, res)) = jobs.next().await {
        match res {
            Ok(()) => compressed += 1,
            Err(err) => log!("[ERROR] Couldn't compress chunk {:?}: {}", chunk_pos, err),
        }
    }

    log!("Compressed {}/{} chunks!", compressed, nb_chunks);
    Ok(())
}

async fn run_crawler(config: config::Config, opts: config::CliCrawl) -> BoxResult<()> {
    use crawler::{ChunkJobResult, CrawlerResult};

    let db = db::Config::from(config.database).open().await?;

    log!("Loading zone map...");
    let chunk_map = crawler::load_chunk_map(&db, &opts.chunks).await?;

    log!("Connecting to Kube server...");
    let (kube_receiver, kube_sender) = connect_kube(&config.twin_session_id, &opts.tora).await?;
    let kube_receiver = Mutex::new(kube_receiver);
    let kube_html = connect_kube_html(&config.twin_session_id, &opts.html)?;

    // Used to notify the sender that a request completed (successfully or not).
    let (completion_sender, completion_receiver) = tokio::sync::mpsc::unbounded_channel();

    // Chunks to crawl, with throttling to not spam the Kube server too much
    let chunks = crawler::stream_chunks(chunk_map, completion_receiver);
    let chunks = tokio_stream::StreamExt::throttle(chunks, opts.throttle_requests)
        .inspect(|chunk| log!("--> {:?}", chunk));
    futures::pin_mut!(chunks);

    // Do network operations
    let requests = chunks.map(|chunk| crawler::request_chunk(&kube_sender, chunk));
    let responses = requests.map(|request_fut| {
        let receiver = &kube_receiver;
        async move {
            request_fut.await?;
            crawler::receive(&mut *receiver.lock().await).await
        }
    });
    // Limit the number of inflight requests
    let responses = responses.buffer_unordered(opts.max_inflight_requests as usize);

    // Process responses concurrently
    let outputs = responses.map(|result| {
        let db = db.clone();
        let html = kube_html.clone();

        tokio::spawn(async move {
            let answer = result?;
            let processed = crawler::process_chunk_with_zones(answer, &html).await?;
            let pos = (processed.chunk.cx, processed.chunk.cy);
            let (status, update) = crawler::save_chunk(&db, processed).await?;
            Ok((pos, status, update))
        })
        .map(|v| v.expect("processing task panicked"))
    });
    // Limit the number of inflight processes
    let outputs = outputs.buffer_unordered(opts.max_inflight_processes as usize);

    let mut total_chunks = 0usize;
    let mut total_errors = 0usize;
    let mut chunks_changed = 0usize;
    let mut chunks_created = 0usize;
    let mut chunks_deleted = 0usize;

    {
        futures::pin_mut!(outputs);
        // Drive the stream to completion
        while let Some(response) = outputs.next().await {
            let response: CrawlerResult<_> = response;
            let result = match response {
                Ok((pos, status, update)) => {
                    total_chunks += 1;
                    match update {
                        db::ChunkUpdate::Unchanged => (),
                        db::ChunkUpdate::Changed => chunks_changed += 1,
                        db::ChunkUpdate::Created => chunks_created += 1,
                        db::ChunkUpdate::Deleted => chunks_deleted += 1,
                    }
                    ChunkJobResult::Completed(pos, status, update)
                }
                Err(err) if err.fatal => return Err(err.into()),
                Err(err) => {
                    total_errors += 1;
                    log!("[ERROR] {}", err);
                    ChunkJobResult::Failed
                }
            };
            log!("<-- {:?}", result);
            completion_sender.send(result).unwrap();
        }
    }

    log!("Finished with {total_errors} errors!");
    log!("Crawled {total_chunks} chunks ({chunks_changed} updated, {chunks_created} created, {chunks_deleted} deleted).");

    kube_sender.close().await?;
    db.close().await;
    Ok(())
}

async fn render_overview(config: config::Config, opts: config::CliOverview) -> BoxResult<()> {
    let db = db::Config::from(config.database).open().await?;

    let bounds = db.get_zone_bounds(db::ZoneBoundsKind::All).await?;
    let bounds =
        ZoneBounds::from_bounds_inclusive(bounds.xmin, bounds.ymin, bounds.xmax, bounds.ymax)
            .unwrap();
    log!(
        "Rendering overview ({}x{} zones)...",
        bounds.width(),
        bounds.height()
    );
    let image = if opts.zoom {
        overview::render_image_zoomed(&db, bounds).await?
    } else {
        overview::render_image(&db, bounds).await?
    };

    log!("Saving image to {}...", opts.output.display());
    tokio::task::block_in_place(|| Ok(image.save(opts.output)?))
}

async fn render_minimap(config: config::Config, opts: config::CliMinimap) -> BoxResult<()> {
    let db = db::Config::from(config.database).open().await?;

    log!("Rendering minimap...");
    let image = minimap::render_image(&db, &opts).await?;

    log!("Saving image to {}...", opts.output.display());
    tokio::task::block_in_place(|| Ok(image.save(opts.output)?))
}

async fn export_raw(config: config::Config, opts: config::CliExportRaw) -> BoxResult<()> {
    let db = db::Config::from(config.database).open().await?;

    log!("Retrieving chunk data...");
    let chunk = db.get_chunk(opts.chunk, opts.date).await?.and_then(|c| c.data);
    
    if let Some(chunk) = chunk {
        log!("Saving raw chunk data to {}...", opts.output.display());
        tokio::task::block_in_place(|| {
            let mut writer = std::fs::File::create(opts.output)?;
            std::io::Write::write_all(&mut writer, chunk.as_slice())?;
            Ok(())  
        })
    } else {
        Err("no chunk exists at this position".into())
    }
}

async fn export_kub3dit(config: config::Config, opts: config::CliExportKub3dit) -> BoxResult<()> {
    use kube_core::export::kub3dit::{ExportKind, ExportOptions};
    let db = db::Config::from(config.database).open().await?;

    let max_size =
        (kube_core::export::kub3dit::MAX_EXPORT_SIZE / kube_core::region::CHUNK_WIDTH) as u32;
    if opts.bounds.width() > max_size || opts.bounds.height() > max_size {
        return Err(format!(
            "region is too big! (maximum: {}x{} zones)",
            max_size, max_size
        )
        .into());
    }

    log!("Retrieving chunk data...");
    let region = region::fetch_region_from_db(&db, opts.bounds, opts.date).await?;

    log!("Exporting data to {}...", opts.output.display());
    tokio::task::block_in_place(|| {
        let writer = std::fs::File::create(opts.output)?;
        let options = ExportOptions {
            kind: match (opts.kind_raw, opts.kind_no_minimap) {
                (true, _) => ExportKind::Raw,
                (false, true) => ExportKind::WithIcon,
                (false, false) => ExportKind::WithMinimap,
            },
        };
        kube_core::export::export_kub3dit(writer, &region, &options)?;
        Ok(())
    })
}

async fn list_forums(config: config::Config, opts: config::CliListForums) -> BoxResult<()> {
    let db = db::Config::from(config.database).open().await?;

    let image_format = opts.format;
    let orientation = opts.orientation;
    let mut output_path = opts.output;
    let output_ext = image_format.extensions_str().get(0).copied();
    let chunk_date = opts.date;

    let mut forums = db
        .get_chunks_status()
        .map(|info| async {
            let chunk_pos = match info {
                Ok(info) if info.generated => (info.cx, info.cy),
                Ok(_) => return Ok(stream::empty().boxed()),
                Err(err) => return BoxResult::Err(err.into()),
            };

            let chunk = db
                .get_chunk(chunk_pos, chunk_date)
                .await?
                .and_then(|chunk| chunk.data);

            let chunk = match chunk {
                Some(chunk) => chunk,
                None => return Ok(stream::empty().boxed()),
            };

            tokio::task::spawn_blocking(move || {
                let chunk = chunk.into_blocks();
                let forums = forums::find_forums_in_chunk(chunk_pos, &chunk);
                let rendered = forums::render_minimaps_with_forums(
                    forums.into_iter(),
                    chunk_pos,
                    &chunk,
                    orientation,
                    image_format,
                )
                .map(|(pos, result)| match result {
                    Ok(buf) => Ok((pos, buf)),
                    Err(err) => BoxResult::Err(err.into()),
                });
                Ok(stream::iter(rendered).boxed())
            })
            .await
            .expect("task failed")
        })
        .buffer_unordered(opts.parallel_jobs.into())
        .try_flatten();

    let mut idx = 0;
    const FILES_PER_SUBDIR: usize = 1000;
    while let Some((forum, rendered)) = forums.try_next().await? {
        output_path.push(format!("{}", idx / FILES_PER_SUBDIR));
        tokio::fs::create_dir_all(&output_path).await?;
        output_path.push(format!(
            "Forum [{}, {}] at ({}, {}, {})",
            forum.zone_x, forum.zone_y, forum.kube_x, forum.kube_y, forum.kube_z
        ));
        output_ext.map(|ext| output_path.set_extension(ext));

        tokio::fs::write(&output_path, &rendered).await?;

        output_path.pop();
        output_path.pop();
        idx += 1;

        if idx % FILES_PER_SUBDIR == 0 {
            log!("Forums found: {}...", idx);
        }
    }

    log!("Total files written: {}", idx);
    Ok(())
}

async fn enable_forums(config: config::Config, opts: config::CliEnableForum) -> BoxResult<()> {
    use kube_core::block::Block;
    use kube_core::region::{DynDim, ZONES_PER_CHUNK, ZONE_WIDTH};

    let (zx, zy) = opts.zone;
    let (cx, cy) = (
        zx.div_euclid(ZONES_PER_CHUNK as i32),
        zy.div_euclid(ZONES_PER_CHUNK as i32),
    );
    let kx = ZONE_WIDTH * zx.rem_euclid(ZONES_PER_CHUNK as i32) as usize;
    let ky = ZONE_WIDTH * zy.rem_euclid(ZONES_PER_CHUNK as i32) as usize;

    let (mut kube_receiver, kube_sender) = connect_kube(
        &config.twin_session_id,
        &config::ToraSettings { max_connections: 1 },
    )
    .await?;

    crawler::request_chunk(&kube_sender, (cx, cy)).await?;

    let raw_chunk = crawler::receive(&mut kube_receiver).await?;
    let forum: BoxResult<_> = tokio::task::block_in_place(|| {
        let chunk = match crawler::process_chunk(raw_chunk)? {
            db::DbChunk {
                data: Some(data), ..
            } => data,
            _ => return Ok(None),
        };

        let mut zone = RegionStorage::<u8, _>::new_sized(DynDim::new(ZONE_WIDTH, ZONE_WIDTH));
        zone.copy_region(
            &chunk,
            (0, 0, 0),
            (kx..kx + ZONE_WIDTH, ky..ky + ZONE_WIDTH, ..),
        );
        let zone = zone.into_blocks();

        let forum = zone
            .blocks_global((
                zx as i64 * ZONE_WIDTH as i64,
                zy as i64 * ZONE_WIDTH as i64,
                0,
            ))
            .find(|(_, block)| *block == &Block::Message)
            .map(|(idx, _)| idx);
        Ok(forum)
    });
    let forum = forum?.ok_or_else(|| format!("couldn't find forum in zone [{}, {}]", zx, zy))?;

    crawler::request_kube_activation(&kube_sender, forum).await?;
    let activation = crawler::receive(&mut kube_receiver).await?;

    log!(
        "Forum enabled: {}",
        crawler::process_kube_forum_activation(activation)?
    );
    Ok(())
}

async fn teleport(config: config::Config, opts: config::CliTeleport) -> BoxResult<()> {
    use kube_core::region::ZONE_WIDTH;
    let (mut kube_receiver, kube_sender) = connect_kube(
        &config.twin_session_id,
        &config::ToraSettings { max_connections: 1 },
    )
    .await?;

    crawler::request_teleport(
        &kube_sender,
        (
            opts.zone.0 as i64 * ZONE_WIDTH as i64 + opts.pos.0 as i64,
            opts.zone.1 as i64 * ZONE_WIDTH as i64 + opts.pos.1 as i64,
            opts.pos.2 as i64,
        ),
    )
    .await?;

    crawler::process_teleport(crawler::receive(&mut kube_receiver).await?)?;
    log!("Teleported to zone [{}, {}]!", opts.zone.0, opts.zone.1);
    Ok(())
}

async fn connect_kube(
    twin_session_id: &TwinSessionId,
    settings: &config::ToraSettings,
) -> BoxResult<(KubeReceiver, KubeSender)> {
    let resolver = session::SessionResolver::new()?;
    let session = resolver.resolve(twin_session_id).await?;

    let user_data: kube_core::msg::MapUserData = haxeformat::from_slice(&session.data)?;

    let server: url::Url = user_data.server.parse()?;
    Ok(kube::connect(kube::ConnectOptions {
        url: &server,
        session: &session,
        max_connections: settings.max_connections as usize,
    })
    .await?)
}

fn connect_kube_html(
    twin_session_id: &TwinSessionId,
    settings: &config::KubeHtmlSettings,
) -> BoxResult<KubeHtml> {
    let options = KubeHtmlOptions {
        session: twin_session_id,
        max_connections: settings.max_connections as usize,
        retry_attempts: settings.retry_attempts as usize,
    };
    Ok(KubeHtml::new(options)?)
}
