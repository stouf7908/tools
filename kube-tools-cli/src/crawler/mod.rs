mod chunks;

use std::{fmt, sync::Arc};

use chrono::{DateTime, Utc};
use futures::prelude::*;
use thiserror::Error;
use tokio::sync::mpsc::UnboundedReceiver;

use kube_core::region::{ChunkBytes, ZONES_PER_CHUNK};
use kube_core::{
    msg::{ServerAnswer, ServerCmd, ZoneInfo},
    region::BlockPos,
};
use kube_db::{ChunkUpdate, DbChunk, DbZone, KubeDb};
use twin_network::tora::ToraError;

use crate::config;
use crate::kube::{KubeHtml, KubeReceiver, KubeSender};
use crate::utils::BoxError;

pub use chunks::*;

#[derive(Debug, Error)]
pub struct CrawlerError {
    msg: String,
    #[source]
    error: Option<BoxError>,
    pub fatal: bool,
}

impl fmt::Display for CrawlerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.error {
            Some(err) => write!(f, "{}: {}", self.msg, err),
            None => f.write_str(&self.msg),
        }
    }
}

pub type CrawlerResult<T> = Result<T, CrawlerError>;

pub async fn load_chunk_map(
    db: &KubeDb,
    settings: &config::UpdateChunksSettings,
) -> kube_db::Result<ChunkStatusMap> {
    let start_chunk = settings.start_chunk.unwrap_or((0, 0));
    let mut map = ChunkStatusMap::new(start_chunk);
    let statuses = db.get_chunks_status().try_filter(|status| {
        future::ready(if let Some(date) = settings.update_chunks_older_than {
            status.last_checked_at > date
        } else {
            true
        })
    });

    futures::pin_mut!(statuses);
    while let Some(status) = statuses.try_next().await? {
        let chunk = (status.cx, status.cy);
        if status.generated {
            map.set_status(chunk, ChunkStatus::Generated);
        } else if Some(chunk) != settings.start_chunk {
            // Only mark the chunk as ungenerated if it isn't the starting chunk.
            map.set_status(chunk, ChunkStatus::Ungenerated);
        }
    }

    map.sort_missing_chunks_by(|(cx, cy)| {
        let dx = *cx as i64 - start_chunk.0 as i64;
        let dy = *cy as i64 - start_chunk.1 as i64;
        dx.abs() + dy.abs()
    });
    Ok(map)
}

#[derive(Debug, Copy, Clone)]
pub enum ChunkJobResult {
    Completed(ChunkPos, ChunkStatus, ChunkUpdate),
    Failed,
}

pub fn stream_chunks(
    chunk_map: ChunkStatusMap,
    notify_completion: UnboundedReceiver<ChunkJobResult>,
) -> impl Stream<Item = ChunkPos> {
    struct State {
        chunk_map: ChunkStatusMap,
        notify: UnboundedReceiver<ChunkJobResult>,
        next_notify: Option<ChunkJobResult>,
        cur_batch: <Vec<ChunkPos> as IntoIterator>::IntoIter,
        num_pending: usize,
    }

    impl State {
        fn process_pending_notifications(&mut self) -> usize {
            let mut num = 0;
            let mut notif = self
                .next_notify
                .take()
                .or_else(|| self.notify.try_recv().ok());

            while self.num_pending > 0 {
                match notif {
                    Some(ChunkJobResult::Completed(chunk, status, _)) => {
                        self.num_pending -= 1;
                        self.chunk_map.set_status(chunk, status);
                    }
                    Some(ChunkJobResult::Failed) => self.num_pending -= 1,
                    None => break,
                }

                notif = self.notify.try_recv().ok();
                num += 1;
            }
            self.next_notify = notif;
            num
        }

        fn next_chunk(&mut self) -> Option<ChunkPos> {
            self.cur_batch
                .next()
                .or_else(|| {
                    self.cur_batch = self.chunk_map.missing_chunks().into_iter();
                    self.cur_batch.next()
                })
                .map(|chunk| {
                    self.num_pending += 1;
                    // Temporarily mark the chunk as errored to
                    // remove it from the missing chunks list.
                    self.chunk_map.set_status(chunk, ChunkStatus::Errored);
                    chunk
                })
        }
    }

    stream::unfold(
        State {
            chunk_map,
            notify: notify_completion,
            next_notify: None,
            cur_batch: Vec::new().into_iter(),
            num_pending: 0,
        },
        |mut state| async {
            state.process_pending_notifications();

            let chunk = loop {
                if let Some(chunk) = state.next_chunk() {
                    break chunk;
                }

                if state.num_pending == 0 {
                    // No pending notifications, we are done
                    return None;
                }

                // Wait for new notifications and try again
                assert!(state.next_notify.is_none());
                state.next_notify = state.notify.recv().await;

                // Check that the channel didn't close
                state.next_notify?;
                state.process_pending_notifications();
            };

            Some((chunk, state))
        },
    )
}

pub async fn request_chunk(sender: &KubeSender, chunk: ChunkPos) -> CrawlerResult<()> {
    let cmd = ServerCmd::Watch {
        mx: chunk.0,
        my: chunk.1,
    };
    sender.send(&cmd).await.map_err(|err| CrawlerError {
        msg: format!("Failed to send request for chunk {:?}", chunk),
        fatal: is_tora_error_fatal(&err),
        error: Some(err.into()),
    })
}

pub async fn request_kube_activation(sender: &KubeSender, pos: BlockPos) -> CrawlerResult<()> {
    let cmd = ServerCmd::ActiveKube {
        x: pos.0,
        y: pos.1,
        z: pos.2,
    };
    sender.send(&cmd).await.map_err(|err| CrawlerError {
        msg: format!("Failed to send request for kube activation {:?}", pos),
        fatal: is_tora_error_fatal(&err),
        error: Some(err.into()),
    })
}

pub async fn request_teleport(sender: &KubeSender, pos: BlockPos) -> CrawlerResult<()> {
    let cmd = ServerCmd::Teleport {
        x: pos.0 * kube_core::msg::SUBUNIT_PER_BLOCK,
        y: pos.1 * kube_core::msg::SUBUNIT_PER_BLOCK,
        z: pos.2 * kube_core::msg::SUBUNIT_PER_BLOCK,
        s: None,
    };
    sender.send(&cmd).await.map_err(|err| CrawlerError {
        msg: format!("Failed to send request for position change {:?}", pos),
        fatal: is_tora_error_fatal(&err),
        error: Some(err.into()),
    })
}

pub async fn receive(receiver: &mut KubeReceiver) -> CrawlerResult<ServerAnswer> {
    match receiver.receive().await {
        Ok(Some(resp)) => Ok(resp),
        Ok(None) => Err(CrawlerError {
            msg: "Failed to read response: connection unexpectedly closed".into(),
            fatal: true,
            error: None,
        }),
        Err(err) => Err(CrawlerError {
            msg: "Failed to read response".into(),
            fatal: is_tora_error_fatal(&err),
            error: Some(err.into()),
        }),
    }
}

fn is_tora_error_fatal(err: &ToraError) -> bool {
    matches!(err, ToraError::ConnectionError(_) | ToraError::IoError(_))
}

pub fn process_teleport(response: ServerAnswer) -> CrawlerResult<()> {
    match response {
        ServerAnswer::PosSaved => Ok(()),
        other => Err(CrawlerError {
            msg: format!("Unexpected server response: {:?}", other),
            fatal: false,
            error: None,
        }),
    }
}

pub fn process_kube_forum_activation(activation: ServerAnswer) -> CrawlerResult<url::Url> {
    const LINK_START: &str = "<a href=\"";
    const LINK_BASE: &str = "http://kube.muxxu.com/";

    let text = match activation {
        ServerAnswer::Message { text, error: false } => text,
        ServerAnswer::Message { text, error: true } => {
            return Err(CrawlerError {
                msg: text,
                fatal: false,
                error: None,
            })
        }
        other => {
            return Err(CrawlerError {
                msg: format!("Unexpected server response: {:?}", other),
                fatal: false,
                error: None,
            })
        }
    };

    let link = match text.find(LINK_START) {
        None => {
            return Err(CrawlerError {
                msg: format!("Couldn't find forum link in message: {}", text),
                fatal: false,
                error: None,
            })
        }
        Some(start) => text[start + LINK_START.len()..]
            .split('"')
            .next()
            .expect("expect non-empty split"),
    };

    let url = url::Url::parse(LINK_BASE).expect("valid site name");
    url.join(link).map_err(|err| CrawlerError {
        msg: format!("Invalid link in message: {}", text),
        fatal: false,
        error: Some(err.into()),
    })
}

pub struct ProcessedChunk {
    pub chunk: DbChunk,
    pub zones: Vec<DbZone>,
}

pub fn process_chunk(chunk: ServerAnswer) -> CrawlerResult<DbChunk> {
    ProcessChunkTasks::from_answer(chunk)?.decompress_chunk_data()
}

pub async fn process_chunk_with_zones(
    chunk: ServerAnswer,
    html: &KubeHtml,
) -> CrawlerResult<ProcessedChunk> {
    let tasks = Arc::new(ProcessChunkTasks::from_answer(chunk)?);
    let tasks2 = Arc::clone(&tasks);

    let decompress_fut = tokio::task::spawn_blocking(move || tasks.decompress_chunk_data())
        .map(|v| v.expect("chunk decompress task panicked"));

    let zones = tasks2.retreive_chunk_zones(html).await?;
    let chunk = decompress_fut.await?;
    Ok(ProcessedChunk { chunk, zones })
}

struct ProcessChunkTasks {
    chunk_pos: ChunkPos,
    data: Option<Vec<u8>>,
    patches: Option<Vec<u8>>,
    zones: Option<Vec<Option<ZoneInfo>>>,
}

impl ProcessChunkTasks {
    fn from_answer(answer: ServerAnswer) -> CrawlerResult<Self> {
        match answer {
            ServerAnswer::Generate { mx, my } => Ok(ProcessChunkTasks {
                chunk_pos: (mx, my),
                data: None,
                patches: None,
                zones: None,
            }),
            ServerAnswer::Map {
                mx,
                my,
                data,
                patches,
                zones,
            } => Ok(ProcessChunkTasks {
                chunk_pos: (mx, my),
                data: Some(data),
                patches,
                zones: Some(zones),
            }),
            other => Err(Self::error(
                format!("Unexpected server response: {:?}", other),
                None,
            )),
        }
    }

    fn error(msg: String, source: Option<BoxError>) -> CrawlerError {
        CrawlerError {
            msg,
            fatal: false,
            error: source,
        }
    }

    fn decompress_chunk_data(&self) -> CrawlerResult<DbChunk> {
        let created_at = Utc::now();
        let (cx, cy) = self.chunk_pos;

        let mut chunk = match &self.data {
            Some(data) => ChunkBytes::decompress(data).map_err(|e| {
                Self::error(
                    format!("Invalid chunk data for {:?}", (cx, cy)),
                    Some(e.into()),
                )
            })?,
            None => {
                return Ok(DbChunk {
                    cx,
                    cy,
                    created_at,
                    data: None,
                })
            }
        };

        if let Some(patches) = &self.patches {
            if !apply_chunk_patches(&mut chunk, patches) {
                return Err(Self::error(
                    format!("Invalid chunk patches for {:?}", (cx, cy)),
                    None,
                ));
            }
        }

        Ok(DbChunk {
            cx,
            cy,
            created_at,
            data: Some(chunk),
        })
    }

    async fn retreive_chunk_zones(&self, html: &KubeHtml) -> CrawlerResult<Vec<DbZone>> {
        let (cx, cy) = self.chunk_pos;
        let zones = match &self.zones {
            Some(zones) => zones,
            None => return Ok(Vec::new()),
        };

        let zones_len = zones.len();
        let zones_max_len = ZONES_PER_CHUNK * ZONES_PER_CHUNK;
        if zones_len > zones_max_len {
            return Err(Self::error(
                format!(
                    "Invalid zone data for {:?}: too many zones in array ({})",
                    (cx, cy),
                    zones.len()
                ),
                None,
            ));
        }
        let zones = zones
            .iter()
            .chain(std::iter::repeat(&None).take(zones_max_len - zones_len))
            .enumerate()
            .map(|(i, zone)| async move {
                const WIDTH: i32 = ZONES_PER_CHUNK as i32;
                let x = i as i32 / WIDTH;
                let y = i as i32 % WIDTH;
                let x = x + cx * WIDTH;
                let y = y + cy * WIDTH;

                let zone_created_at = Utc::now();

                match zone {
                    None => Ok(DbZone::empty(x, y, zone_created_at)),
                    Some(zone) => html.get_zone_info(x, y).await.map(|html_zone| DbZone {
                        x,
                        y,
                        created_at: zone_created_at,
                        location_end: zone.date.map(|d| DateTime::from_utc(d.into(), Utc)),
                        owner: html_zone.owner.or_else(|| zone.user.clone()),
                        owner_id: html_zone.owner_id,
                        name: html_zone.name.or_else(|| zone.name.clone()),
                        description: html_zone.description,
                    }),
                }
            });

        future::join_all(zones)
            .await
            .into_iter()
            .collect::<Result<Vec<_>, _>>()
            .map_err(|err| {
                Self::error(
                    format!("Invalid zone data for {:?}", (cx, cy)),
                    Some(err.into()),
                )
            })
    }
}

fn apply_chunk_patches(chunk: &mut ChunkBytes, patches: &[u8]) -> bool {
    let mut patches = patches.chunks_exact(4);
    for patch in &mut patches {
        let (x, y, z, block) = (patch[0].into(), patch[1].into(), patch[2].into(), patch[3]);
        if z == 0 {
            break;
        }
        match chunk.get_mut((x, y, z)) {
            Some(dst) => *dst = block,
            None => return false,
        }
    }
    patches.remainder().is_empty()
}

pub async fn save_chunk(
    db: &KubeDb,
    chunk: ProcessedChunk,
) -> CrawlerResult<(ChunkStatus, ChunkUpdate)> {
    let (generated, cx, cy) = (chunk.chunk.data.is_some(), chunk.chunk.cx, chunk.chunk.cy);

    let res = db.save_chunk_and_zones(&chunk.chunk, &chunk.zones).await;
    match res {
        Ok(update) => {
            if generated {
                Ok((ChunkStatus::Generated, update))
            } else {
                Ok((ChunkStatus::Ungenerated, update))
            }
        }
        Err(err) => Err(CrawlerError {
            msg: format!("Failed to save chunk {:?}", (cx, cy)),
            fatal: false,
            error: Some(err.into()),
        }),
    }
}
