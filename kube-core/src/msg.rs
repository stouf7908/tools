use bitflags::bitflags;
use serde::{Deserialize, Serialize};

pub const SUBUNIT_PER_BLOCK: i64 = 16;

#[derive(Debug, Copy, Clone)]
pub struct UnknownValue;

impl Serialize for UnknownValue {
    fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_unit_struct("UnknownValue")
    }
}

impl<'de> Deserialize<'de> for UnknownValue {
    fn deserialize<D: serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        serde::de::IgnoredAny::deserialize(deserializer)?;
        Ok(UnknownValue)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Replay {
    /// Content of (0,0) chunk
    #[serde(rename = "_z0")]
    pub z0: Vec<u8>,
    /// Content of (1,0) chunk
    #[serde(rename = "_z1")]
    pub z1: Vec<u8>,
    /// Content of (0,1) chunk
    #[serde(rename = "_z2")]
    pub z2: Vec<u8>,
    /// Content of (1,1) chunk
    #[serde(rename = "_z3")]
    pub z3: Vec<u8>,
}

bitflags! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub struct CapabilitiesFlags: u32 {
        const LAVA_SUIT = 0b00000001;
        const COMPASS = 0b00000010;
        const CAMERA = 0b00000100;
        const CAMERA2 = 0b00001000;
        const CAMERA3 = 0b00010000;
        const SWIM_SUIT = 0b00100000;
        const UNKNOWN = 0b01000000;
        /// Allow to take/put BFixed anywhere + press 'U' to display current ping
        const ADMIN = 0b1_00000000_00000000;
    }
}

impl Serialize for CapabilitiesFlags {
    fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_u32(self.bits())
    }
}

impl<'de> Deserialize<'de> for CapabilitiesFlags {
    fn deserialize<D: serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let bits: u32 = Deserialize::deserialize(deserializer)?;
        let value = CapabilitiesFlags::from_bits_truncate(bits);
        Ok(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PlayUserData {
    #[serde(rename = "_s")]
    pub server: String,
    #[serde(rename = "_uid")]
    pub user_id: u32,
    #[serde(rename = "_force")]
    pub force: bool, // TODO: what is this?
    #[serde(rename = "_tuto")]
    pub tuto: usize, // current step of the tutorial
    #[serde(rename = "_pow")]
    pub power: u16,
    #[serde(rename = "_swim")]
    pub swim: Option<u16>,
    #[serde(rename = "_replay")]
    pub replay: Option<Replay>, // demo mode
    #[serde(rename = "_imax")]
    pub inventory_max_size: u8,
    #[serde(rename = "_flags")]
    pub flags: CapabilitiesFlags,
    #[serde(rename = "_u")]
    pub universe: i32,
    #[serde(rename = "_x")]
    pub pos_x: i32,
    #[serde(rename = "_y")]
    pub pos_y: i32,
    #[serde(rename = "_z")]
    pub pos_z: i32,
    #[serde(rename = "_inv")]
    pub inventory: Vec<Option<u8>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MapUserData {
    #[serde(rename = "_s")]
    pub server: String,
    #[serde(rename = "_x")]
    pub pos_x: i32,
    #[serde(rename = "_y")]
    pub pos_y: i32,
    #[serde(rename = "_dol", with = "haxeformat::serde::adapter::list")]
    pub unknown: UnknownValue,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
#[serde(rename = "_GO")]
pub enum DeathCause {
    Water,
    Lava,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename = "_Cmd")]
pub enum ServerCmd {
    Watch {
        mx: i32,
        my: i32,
    },
    GenLevel {
        mx: i32,
        my: i32,
        data: Vec<u8>,
    },
    SetBlocks((i64, i64, i64, u8, u8)),
    SavePos {
        x: i64,
        y: i64,
        z: i64,
        s: Option<u32>,
    },
    Unknown04 {
        mx: i32,
        my: i32,
    },
    Load {
        mx: i32,
        my: i32,
    },
    Regen {
        u: i32,
        zx: i32,
        zy: i32,
        block: u8,
    },
    GameOver(DeathCause),
    DoCheck {
        px: i64,
        py: i64,
        pz: i64,
    },
    Unknown09 {
        zx1: i32,
        zy1: i32,
        zx2: i32,
        zy2: i32,
        qty: u32,
        k: i32,
        water: bool,
        mine: bool,
    },
    Ping(u64),
    SavePhoto {
        zx: i32,
        zy: i32,
        big: String,
        small: String,
        hide: bool,
    },
    ActiveKube {
        x: i64,
        y: i64,
        z: i64,
    },
    UpdateTuto(usize),
    GetStatus,
    Teleport {
        x: i64,
        y: i64,
        z: i64,
        s: Option<u32>,
    },
    Undo,
    CountBlocks {
        zu: i32,
        zx: i32,
        zy: i32,
    },
    Fix {
        mu: i32,
        mx: i32,
        my: i32,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ZoneInfo {
    #[serde(rename = "_d", with = "haxeformat::serde::adapter::date")]
    pub date: Option<haxeformat::HaxeDate>,
    #[serde(rename = "_n")]
    pub name: Option<String>,
    #[serde(rename = "_u")]
    pub user: Option<String>,
    #[serde(rename = "_p")]
    pub put_permissions: Option<bool>,
    #[serde(rename = "_g")]
    pub get_permissions: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ValueObject {
    Fix { water: i32, invalid: i32 },
    CountBlocks { count: i32, surf: i32 },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename = "_Answer")]
pub enum ServerAnswer {
    Blocks(Vec<Option<u8>>),
    Set {
        x: i64,
        y: i64,
        z: i64,
        k: u8,
    },
    PosSaved,
    Map {
        mx: i32,
        my: i32,
        #[serde(with = "serde_bytes")]
        data: Vec<u8>,
        #[serde(with = "serde_bytes")]
        patches: Option<Vec<u8>>,
        zones: Vec<Option<ZoneInfo>>,
    },
    Generate {
        mx: i32,
        my: i32,
    },
    Redirect(String),
    Message {
        text: String,
        error: bool,
    },
    Nothing,
    Value {
        v: ValueObject,
    },
    ShowError(String),
    Pong(i32),
    SetMany(Vec<i64>), // TODO: Vec<SetBlock>
}
