use std::io::{self, Read};

use crate::region::RegionError;

#[derive(Debug, Copy, Clone)]
pub enum CompressionMethod {
    Zlib,
    Rle,
    RleDeflate,
}

impl CompressionMethod {
    fn detect(data: &[u8]) -> Result<Self, RegionError> {
        match data {
            [0x78, ..] => Ok(Self::Zlib),
            [rle::RLE_HEADER, ..] => Ok(Self::Rle),
            [rle::RLE_DEFLATE_HEADER, ..] => Ok(Self::RleDeflate),
            _ => Err(RegionError::Decompress(make_io_error(
                "invalid chunk header",
            ))),
        }
    }
}

pub fn compress_into(chunk: &[u8], buf: &mut Vec<u8>, method: CompressionMethod) {
    buf.reserve(BUF_START_CAP);
    match method {
        CompressionMethod::Zlib => zlib::compress(chunk, buf),
        CompressionMethod::Rle => rle::compress(chunk, buf),
        CompressionMethod::RleDeflate => rle::compress_deflate(chunk, buf),
    }
}

pub fn decompress_in_place(chunk: &mut [u8], compressed: &[u8]) -> Result<(), RegionError> {
    match CompressionMethod::detect(compressed)? {
        CompressionMethod::Zlib => zlib::decompress(chunk, compressed)?,
        CompressionMethod::Rle => rle::decompress(chunk, compressed)?,
        CompressionMethod::RleDeflate => rle::decompress_deflate(chunk, compressed)?,
    }
    Ok(())
}

fn make_io_error(msg: &'static str) -> io::Error {
    io::Error::new(io::ErrorKind::InvalidInput, msg)
}

struct TruncateRead<R> {
    reader: R,
    remaining: usize,
}

impl<R: Read> Read for TruncateRead<R> {
    fn read(&mut self, mut buf: &mut [u8]) -> io::Result<usize> {
        if buf.len() > self.remaining {
            buf = &mut buf[..self.remaining];
        }
        let len = self.reader.read(buf)?;
        self.remaining -= len;
        Ok(len)
    }
}

const BUF_START_CAP: usize = 2048;

mod zlib {
    use super::*;

    use flate2::bufread::{ZlibDecoder, ZlibEncoder};
    use std::io::Read;

    pub const COMPRESSION_LEVEL: u32 = 6;

    pub fn compress(chunk: &[u8], buf: &mut Vec<u8>) {
        let level = flate2::Compression::new(COMPRESSION_LEVEL);
        let mut enc = ZlibEncoder::new(std::io::Cursor::new(&chunk), level);
        enc.read_to_end(buf)
            .expect("reading from memory can't fail");
    }

    pub fn decompress(chunk: &mut [u8], compressed: &[u8]) -> Result<(), RegionError> {
        let size = chunk.len();
        let mut dec = ZlibDecoder::new(std::io::Cursor::new(compressed));
        let mut buf = chunk;

        // We can't use read_exact, because we need to differenciate "invalid size" errors
        // and actual UnexpectedEofs from the stream.
        loop {
            match dec.read(buf) {
                Ok(0) => break,
                Ok(read) => buf = &mut buf[read..],
                Err(err) => return Err(RegionError::Decompress(err)),
            }
        }

        if dec.into_inner().position() != compressed.len() as u64 {
            Err(RegionError::Decompress(make_io_error(
                "unexpected trailing data",
            )))
        } else if !buf.is_empty() {
            Err(RegionError::InvalidSize {
                expected: size,
                actual: Some(size - buf.len()),
            })
        } else {
            Ok(())
        }
    }
}

mod rle {
    use super::*;

    use std::io::Read;

    use crate::region::rle::{self, RleDecodeError};
    use flate2::bufread::{DeflateDecoder, DeflateEncoder};

    pub const RLE_HEADER: u8 = 0x10;
    pub const RLE_DEFLATE_HEADER: u8 = 0x11;

    pub fn compress(chunk: &[u8], buf: &mut Vec<u8>) {
        buf.push(RLE_HEADER);
        rle::encode(buf, chunk);
    }

    fn do_decompress(chunk: &mut [u8], compressed: &[u8]) -> Result<(), RegionError> {
        match rle::decode(chunk, compressed) {
            Ok(written) => {
                if written != chunk.len() {
                    Err(RegionError::InvalidSize {
                        expected: chunk.len(),
                        actual: Some(written),
                    })
                } else {
                    Ok(())
                }
            }
            Err(RleDecodeError::BufferTooSmall) => Err(RegionError::InvalidSize {
                expected: chunk.len(),
                actual: None,
            }),
            Err(RleDecodeError::NotEnoughData) => Err(RegionError::Decompress(make_io_error(
                "incomplete RLE stream",
            ))),
        }
    }

    pub fn decompress(chunk: &mut [u8], mut compressed: &[u8]) -> Result<(), RegionError> {
        // Skip header
        if !compressed.is_empty() {
            compressed = &compressed[1..];
        }
        do_decompress(chunk, compressed)
    }

    pub fn compress_deflate(chunk: &[u8], buf: &mut Vec<u8>) {
        buf.push(RLE_DEFLATE_HEADER);

        let mut rle = Vec::with_capacity(BUF_START_CAP);
        compress(chunk, &mut rle);
        let rle = &rle[1..]; // Skip header
        let level = flate2::Compression::new(zlib::COMPRESSION_LEVEL);
        let mut enc = DeflateEncoder::new(std::io::Cursor::new(rle), level);

        enc.read_to_end(buf)
            .expect("reading from memory can't fail");
    }

    pub fn decompress_deflate(chunk: &mut [u8], mut compressed: &[u8]) -> Result<(), RegionError> {
        // Skip header
        if !compressed.is_empty() {
            compressed = &compressed[1..];
        }

        let mut rle = Vec::with_capacity(BUF_START_CAP);
        let mut dec = TruncateRead {
            reader: DeflateDecoder::new(std::io::Cursor::new(compressed)),
            // Allow at most 150% of uncompressed chunk size.
            remaining: chunk.len().saturating_add(chunk.len() / 2),
        };

        dec.read_to_end(&mut rle).map_err(RegionError::Decompress)?;
        if dec.remaining == 0 {
            return Err(RegionError::InvalidSize {
                // Too much decompressed data
                expected: chunk.len(),
                actual: None,
            });
        }

        do_decompress(chunk, &rle)
    }
}
