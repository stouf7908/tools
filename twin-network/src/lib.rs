pub mod codec;
pub mod session;
pub mod tora;

use serde::{de::DeserializeOwned, Serialize};

use codec::CodecKey;
use session::TwinSessionId;
use tora::{Result, ToraAddr, ToraError, ToraReceiverPool, ToraSenderPool};

pub struct ConnectOptions {
    pub addr: ToraAddr,
    pub sid: TwinSessionId,
    pub key: CodecKey,
    pub max_connections: usize,
}

pub async fn connect(options: ConnectOptions) -> Result<(GameReceiver, GameSender)> {
    let (receiver, sender) = options.addr.connect_pool(options.max_connections).await?;
    let (sid, key) = (options.sid, options.key);
    Ok((
        GameReceiver {
            receiver,
            key: key.clone(),
        },
        GameSender { sender, sid, key },
    ))
}

pub struct GameSender {
    sender: ToraSenderPool,
    sid: TwinSessionId,
    key: CodecKey,
}

impl GameSender {
    pub async fn send<T: Serialize>(&self, endpoint: &str, data: &T) -> Result<()> {
        use std::io::Write;

        let (sid, key) = (&self.sid, &self.key);

        self.sender
            .acquire()
            .await?
            .endpoint(endpoint)
            .add_header("Cookie", |f| {
                f.write_all(b"sid=")?;
                f.write_all(sid.as_str().as_bytes())?;
                Ok(())
            })
            .add_param("__d", |f| {
                let ctx = haxeformat::SerializerContext::new().use_enum_index(true);
                haxeformat::to_writer_with(data, &mut *f, ctx)?;

                let crc = key.encode_slice(f.contents_mut());
                f.write_all(&crc)?;
                Ok(())
            })
            .send()
            .await
    }

    pub async fn close(self) -> Result<()> {
        self.sender.close().await
    }
}

pub struct GameReceiver {
    receiver: ToraReceiverPool,
    key: CodecKey,
}

impl GameReceiver {
    pub async fn receive<T: DeserializeOwned>(&mut self) -> Result<Option<T>> {
        self.receiver.advance().await?;
        let key = &self.key;
        let frame = match self.receiver.frame() {
            Some(frame) => frame,
            None => return Ok(None),
        };

        let data = match key.decode_slice(frame) {
            Ok(data) => data,
            // If decoding failed, this may be an error message from the server.
            Err(err) => {
                return Err(match std::str::from_utf8(frame) {
                    Ok(err_msg) => ToraError::RemoteError(err_msg.to_owned()),
                    Err(_) => ToraError::InvalidData(err.into()),
                })
            }
        };

        let result =
            haxeformat::from_slice(data).map_err(|err| ToraError::InvalidData(err.into()))?;
        Ok(Some(result))
    }
}
