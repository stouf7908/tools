use std::sync::atomic::{AtomicUsize, Ordering};

use crossbeam_queue::ArrayQueue;
use futures::future::BoxFuture;
use futures::prelude::*;
use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use tokio::sync::Semaphore;

use super::*;

pub async fn connect_pool(
    addr: &ToraAddr,
    max_connections: usize,
) -> Result<(ToraReceiverPool, ToraSenderPool)> {
    if max_connections == 0 {
        return Err(ToraError::ConnectionError(io::Error::new(
            io::ErrorKind::Other,
            "can't create Tora pool with no connections",
        )));
    }

    let (recv_sender, recv_receiver) = mpsc::unbounded_channel();
    let receiver_pool = ToraReceiverPool::new(recv_receiver);
    let sender_pool = ToraSenderPool::new(addr.clone(), max_connections, recv_sender);

    // Create connection to check addr validity
    sender_pool.acquire().await?;

    Ok((receiver_pool, sender_pool))
}

pub struct ToraSenderPool {
    addr: ToraAddr,

    // INVARIANTS:
    // - remaining_fresh <= available <= max_connections
    // - available - remaining_fresh = senders.len()
    remaining_fresh: AtomicUsize, // Number of fresh connections that can be created
    available: Semaphore, // Available slots in pool (either in queue, or as fresh connections)

    new_recv_futs: UnboundedSender<NextRecvFuture>,
    senders: ArrayQueue<ToraSender>,
}

impl ToraSenderPool {
    fn new(
        addr: ToraAddr,
        max_connections: usize,
        new_recv_futs: UnboundedSender<NextRecvFuture>,
    ) -> Self {
        Self {
            addr,
            remaining_fresh: AtomicUsize::new(max_connections),
            available: Semaphore::new(max_connections),
            new_recv_futs,
            senders: ArrayQueue::new(max_connections),
        }
    }

    pub async fn acquire(&self) -> Result<ToraPooledSender<'_>> {
        // Wait for a connection to be made available
        let guard = self
            .available
            .acquire()
            .await
            .map_err(|_| Self::pool_closed_error())?;

        let sender = if let Some(sender) = self.try_create_fresh_connection().await? {
            sender
        } else if let Some(sender) = self.acquire_from_pool().await {
            sender
        } else {
            panic!("Tora pool underflow")
        };

        guard.forget();
        Ok(self.make_pooled(sender))
    }

    async fn acquire_from_pool(&self) -> Option<ToraSender> {
        while let Some(sender) = self.senders.pop() {
            if sender.is_broken() {
                let _ = sender.close().await;
                // We removed a sender from the pool, put back a 'fresh' token
                self.remaining_fresh.fetch_add(1, Ordering::SeqCst);
            } else {
                return Some(sender);
            }
        }
        None
    }

    async fn try_create_fresh_connection(&self) -> Result<Option<ToraSender>> {
        // CAS loop to acquire a token to create a fresh connection
        let mut fresh = self.remaining_fresh.load(Ordering::SeqCst);
        loop {
            if fresh == 0 {
                return Ok(None); // No token left; we can't create the connection
            }
            let acquired = self.remaining_fresh.compare_exchange_weak(
                fresh,
                fresh - 1,
                Ordering::SeqCst,
                Ordering::SeqCst,
            );
            match acquired {
                // Successfully acquired token; we can create the connection
                Ok(_) => break,
                Err(prev) => fresh = prev,
            }
        }

        match self.do_create_fresh_connection().await {
            Ok(sender) => Ok(Some(sender)),
            Err(err) => {
                // We failed to create the connection, put back the 'fresh' token
                self.remaining_fresh.fetch_add(1, Ordering::SeqCst);
                Err(err)
            }
        }
    }

    async fn do_create_fresh_connection(&self) -> Result<ToraSender> {
        let (receiver, sender) = self.addr.connect().await?;
        //println!("created fresh conn: {}", sender.id());
        self.new_recv_futs
            .send(ToraReceiverPool::future_from_recv(receiver))
            .map_err(|_| Self::pool_closed_error())?;
        Ok(sender)
    }

    pub async fn close(self) -> Result<()> {
        // Drop the channel for new receivers,
        // so that the PooledReceiver can terminate
        drop(self.new_recv_futs);

        // Close all connections concurrently
        let mut close = stream::FuturesUnordered::new();
        while let Some(sender) = self.senders.pop() {
            close.push(sender.close());
        }

        let mut result = Ok(());
        while let Some(r) = close.next().await {
            result = result.and(r);
        }

        result
    }

    fn release_sender(&self, sender: ToraSender) {
        self.senders
            .push(sender)
            .map_err(drop)
            .expect("tora pool overflow");
        self.available.add_permits(1);
    }

    fn make_pooled(&self, sender: ToraSender) -> ToraPooledSender<'_> {
        ToraPooledSender {
            sender: Some(sender),
            pool: self,
        }
    }

    fn pool_closed_error() -> ToraError {
        ToraError::ConnectionError(io::Error::new(
            io::ErrorKind::Other,
            "Tora receiver pool dropped",
        ))
    }
}

pub struct ToraPooledSender<'a> {
    sender: Option<ToraSender>,
    pool: &'a ToraSenderPool,
}

impl<'a> std::ops::Deref for ToraPooledSender<'a> {
    type Target = ToraSender;
    fn deref(&self) -> &Self::Target {
        self.sender.as_ref().unwrap()
    }
}

impl<'a> std::ops::DerefMut for ToraPooledSender<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.sender.as_mut().unwrap()
    }
}

impl<'a> Drop for ToraPooledSender<'a> {
    fn drop(&mut self) {
        // Put back the sender into the pool.
        let sender = self.sender.take().unwrap();
        self.pool.release_sender(sender);
    }
}

type NextRecvFuture = BoxFuture<'static, (Result<()>, ToraReceiver)>;

pub struct ToraReceiverPool {
    new_recv_futs: UnboundedReceiver<NextRecvFuture>,
    pending: stream::FuturesUnordered<NextRecvFuture>,
    current: Option<ToraReceiver>,
}

impl ToraReceiverPool {
    fn new(new_recv_futs: UnboundedReceiver<NextRecvFuture>) -> Self {
        Self {
            new_recv_futs,
            pending: stream::FuturesUnordered::new(),
            current: None,
        }
    }

    pub async fn advance(&mut self) -> Result<()> {
        // Put back the current recv in the list
        if let Some(recv) = self.current.take() {
            self.pending.push(Self::future_from_recv(recv));
        }

        loop {
            // Put available new recvs in the list
            while let Ok(recv_fut) = self.new_recv_futs.try_recv() {
                self.pending.push(recv_fut);
            }

            // Wait for a receiver to receive a response
            return match self.pending.next().await {
                Some((Err(err), recv)) => {
                    if recv.is_broken() {
                        // Unrecoverable error, drop the recv and propagate
                        drop(recv);
                        Err(err)
                    } else {
                        self.current = Some(recv);
                        Err(err)
                    }
                }

                Some((Ok(()), mut recv)) => {
                    if recv.frame().is_some() {
                        self.current = Some(recv);
                        Ok(())
                    } else {
                        // Receiver exhausted, drop it and try again
                        drop(recv);
                        continue;
                    }
                }

                // All receivers are exhausted, retrieve one from
                // the new_recvs channel and try again
                None => match self.new_recv_futs.recv().await {
                    Some(recv_fut) => {
                        self.pending.push(recv_fut);
                        continue;
                    }
                    // new_recvs is exhausted, we are done
                    None => Ok(()),
                },
            };
        }
    }

    pub fn frame(&mut self) -> Option<&mut [u8]> {
        self.current.as_mut().and_then(|recv| recv.frame())
    }

    fn future_from_recv(mut recv: ToraReceiver) -> NextRecvFuture {
        Box::pin(async move {
            let res = recv.advance().await;
            (res, recv)
        })
    }
}
