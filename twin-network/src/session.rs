pub use session_id::*;

mod session_id {
    use std::convert::TryInto;
    use std::fmt;
    use std::str::{self, FromStr};

    use serde::{Deserialize, Deserializer, Serialize, Serializer};
    use thiserror::Error;

    const SESSION_ID_LEN: usize = 32;

    #[derive(Clone)]
    pub struct TwinSessionId([u8; SESSION_ID_LEN]);

    impl fmt::Debug for TwinSessionId {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            f.debug_tuple("TwinSessionId")
                .field(&self.as_str())
                .finish()
        }
    }

    impl TwinSessionId {
        pub fn new(s: &str) -> Result<Self, TwinSessionIdParseError> {
            if s.len() == SESSION_ID_LEN && s.chars().all(char::is_alphanumeric) {
                Ok(Self(s.as_bytes().try_into().unwrap()))
            } else {
                Err(TwinSessionIdParseError(()))
            }
        }

        pub fn as_bytes(&self) -> &[u8] {
            &self.0
        }

        pub fn as_str(&self) -> &str {
            unsafe {
                // Safe: this struct can only be constructed from
                // a valid string and can never be modified
                str::from_utf8_unchecked(self.as_bytes())
            }
        }
    }

    impl FromStr for TwinSessionId {
        type Err = TwinSessionIdParseError;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Self::new(s)
        }
    }

    impl Serialize for TwinSessionId {
        fn serialize<S: Serializer>(&self, ser: S) -> Result<S::Ok, S::Error> {
            ser.serialize_str(self.as_str())
        }
    }

    impl<'de> Deserialize<'de> for TwinSessionId {
        fn deserialize<D: Deserializer<'de>>(de: D) -> Result<Self, D::Error> {
            let s: &str = Deserialize::deserialize(de)?;
            TwinSessionId::new(s).map_err(<D::Error as serde::de::Error>::custom)
        }
    }

    #[derive(Debug, Error)]
    #[error("invalid Twinoid session id")]
    pub struct TwinSessionIdParseError(());
}
