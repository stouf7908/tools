use chrono::{DateTime, Utc};
use futures::prelude::*;

use kube_core::region::ChunkPos;

mod chunks;
mod connect;
mod errors;
mod migrations;
mod zones;

pub use chunks::{ChunkStatus, ChunkUpdate, DbChunk};
pub use connect::Config;
pub use errors::{DbOpenError, Error, Result};
pub use zones::{DbZone, ZoneBounds, ZoneBoundsKind};

type DbPool = sqlx::PgPool;
type DbConn = sqlx::PgConnection;

#[derive(Debug, Clone)]
pub struct KubeDb(DbPool);
impl KubeDb {
    pub fn get_chunks_status(&self) -> impl Stream<Item = Result<ChunkStatus>> + '_ {
        chunks::get_chunks_status(&self.0)
    }

    pub fn get_uncompressed_chunks(&self) -> impl Stream<Item = Result<ChunkPos>> + '_ {
        chunks::get_uncompressed_chunks(&self.0)
    }

    pub fn get_inhabited_zones(&self) -> impl Stream<Item = Result<(i32, i32)>> + '_ {
        zones::get_inhabited_zones(&self.0)
    }

    pub async fn get_zone_bounds(&self, kind: ZoneBoundsKind) -> Result<ZoneBounds> {
        zones::get_zone_bounds(&self.0, kind).await
    }

    pub async fn get_chunk(
        &self,
        pos: ChunkPos,
        date: Option<DateTime<Utc>>,
    ) -> Result<Option<DbChunk>> {
        chunks::get_chunk(&self.0, pos, date).await
    }

    pub async fn save_chunk_and_zones(
        &self,
        chunk: &DbChunk,
        zones: &[DbZone],
    ) -> Result<ChunkUpdate> {
        let mut tx = self.0.begin().await?;
        let update = chunks::save_chunk(&mut *tx, chunk).await?;
        for zone in zones {
            zones::save_zone(&mut *tx, zone).await?;
        }
        tx.commit().await?;
        Ok(update)
    }

    pub async fn compress_chunk(&self, pos: ChunkPos) -> Result<()> {
        let mut tx = self.0.begin().await?;
        chunks::compress_chunk(&mut *tx, pos).await?;
        tx.commit().await?;
        Ok(())
    }

    pub async fn close(self) {
        self.0.close().await
    }
}
