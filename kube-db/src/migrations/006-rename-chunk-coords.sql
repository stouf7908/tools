-- Rename chunk coordinates mx/my to cx/cy

ALTER TABLE chunks RENAME COLUMN mx TO cx;
ALTER TABLE chunks RENAME COLUMN my TO cy;

ALTER TABLE chunks_history RENAME COLUMN mx TO cx;
ALTER TABLE chunks_history RENAME COLUMN my TO cy;

DROP VIEW chunks_full_history;
CREATE VIEW chunks_full_history AS (
    SELECT * FROM chunks_history UNION ALL
    SELECT
        chunks.cx, chunks.cy,
        chunks.data, chunks.compressed,
        FALSE as is_delta,
        chunks.created_at as valid_since
    FROM chunks
);
