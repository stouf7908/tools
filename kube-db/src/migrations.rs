use std::convert::TryInto;

use sqlx::prelude::*;

use super::{DbOpenError, DbPool};

macro_rules! declare_migrations {
    (
        const $migrations:ident = [$($mig_version:literal: $mig_name:literal),* $(,)?];
        const $schema:ident: $schema_version:ident = $schema_file:literal;
    ) => {
        #[allow(clippy::zero_prefixed_literal)]
        const $migrations: &[Migration] = &[$(
            Migration {
                name: $mig_name,
                sql: include_str!(concat!(
                    "./migrations/",
                    stringify!($mig_version), "-", $mig_name,
                    ".sql",
                )),
                version: Some($mig_version),
            }
        ),*];

        const $schema: Migration = Migration {
            name: "create-schema",
            sql: include_str!($schema_file),
            version: Some($schema_version),
        };

        #[allow(clippy::zero_prefixed_literal)]
        pub const $schema_version: i32 = declare_migrations!(@last_of $($mig_version)*);
    };
    (@last_of $v:literal $($rest:literal)+) => { declare_migrations!(@last_of $($rest)+) };
    (@last_of $v:literal) => { $v };
}

declare_migrations! {
    const MIGRATIONS = [
        001: "initial",
        002: "remove-empty-zones",
        003: "more-zone-info",
        004: "chunk-compression",
        005: "history-tables",
        006: "rename-chunk-coords",
    ];
    const CURRENT_SCHEMA: CURRENT_SCHEMA_VERSION = "./schema.sql";
}

const CREATE_SCHEMA_VERSIONS_TABLE: Migration = Migration {
    name: "create-schema-versions-table",
    sql: r#"
        CREATE TABLE _schema_versions (
            version INTEGER NOT NULL,
            date TIMESTAMP NOT NULL
        );

        INSERT INTO _schema_versions VALUES (0, NOW());
    "#,
    version: None,
};

struct Migration {
    name: &'static str,
    sql: &'static str,
    version: Option<i32>,
}

fn get_migrations_for(from: Option<i32>) -> &'static [Migration] {
    match from {
        Some(from) => {
            let start = MIGRATIONS
                .binary_search_by_key(&(from + 1), |e| e.version.unwrap())
                .unwrap_or_else(|i| i);
            &MIGRATIONS[start..]
        }
        None => &[CREATE_SCHEMA_VERSIONS_TABLE, CURRENT_SCHEMA],
    }
}

pub async fn dry_run_migrations(db: &DbPool, from: Option<i32>) -> Result<(), sqlx::Error> {
    let mut tx = db.begin().await?;

    for migration in get_migrations_for(from) {
        eprintln!("Applying migration {}... (dry run)", migration.name); // TODO: use the log or tracing crate
        tx.execute(migration.sql).await?;
    }

    tx.rollback().await?;
    eprintln!("Done!"); // TODO: use the log or tracing crate
    Ok(())
}

pub async fn run_migrations(db: &DbPool, from: Option<i32>) -> Result<(), sqlx::Error> {
    for migration in get_migrations_for(from) {
        eprintln!("Applying migration {}...", migration.name); // TODO: use the log or tracing crate
        let mut tx = db.begin().await?;
        tx.execute(migration.sql).await?;
        if let Some(version) = migration.version {
            sqlx::query("INSERT INTO _schema_versions VALUES ($1, NOW())")
                .bind(version)
                .execute(&mut tx)
                .await?;
        }
        tx.commit().await?;
    }

    eprintln!("Done!"); // TODO: use the log or tracing crate
    Ok(())
}

pub async fn detect_database_schema_version(db: &DbPool) -> Result<Option<i32>, DbOpenError> {
    // We don't use the query! macro here, as we don't actually know the db schema yet
    let (nb_tables,): (i64,) = sqlx::query_as(
        r#"
        SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'public'
    "#,
    )
    .fetch_one(db)
    .await?;

    if nb_tables == 0 {
        return Ok(None);
    }

    let schema_version: Result<(i32,), _> = sqlx::query_as(
        r#"
        SELECT version FROM _schema_versions ORDER BY date DESC LIMIT 1
    "#,
    )
    .fetch_one(db)
    .await;
    match schema_version {
        Ok((schema_version,)) => match schema_version.try_into() {
            Ok(v) => Ok(Some(v)),
            Err(_) => Err(DbOpenError::UnknownSchema),
        },
        Err(sqlx::Error::Database(_)) => Err(DbOpenError::UnknownSchema),
        Err(err) => Err(DbOpenError::DbError(err)),
    }
}
