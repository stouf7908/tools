use chrono::{DateTime, Utc};
use futures::prelude::*;
use sqlx::FromRow;

use crate::{DbConn, DbPool, Result};

#[derive(FromRow)]
// If all optional fields are None, the zone won't be stored in the database
pub struct DbZone {
    pub x: i32,
    pub y: i32,
    pub owner: Option<String>,
    pub owner_id: Option<i32>,
    pub name: Option<String>,
    pub description: Option<String>,
    pub location_end: Option<DateTime<Utc>>,
    pub created_at: DateTime<Utc>,
}

impl DbZone {
    pub fn empty(x: i32, y: i32, created_at: DateTime<Utc>) -> Self {
        Self {
            x,
            y,
            created_at,
            owner: None,
            owner_id: None,
            name: None,
            description: None,
            location_end: None,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.owner.is_none()
            && self.owner_id.is_none()
            && self.name.is_none()
            && self.description.is_none()
            && self.location_end.is_none()
    }

    fn compare(&self, other: &Self) -> bool {
        // Compare all fields except created_at
        self.x == other.x
            && self.y == other.y
            && self.owner == other.owner
            && self.owner_id == other.owner_id
            && self.name == other.name
            && self.description == other.description
            && self.location_end == other.location_end
    }
}

#[derive(Debug, Copy, Clone, sqlx::FromRow)]
pub struct ZoneBounds {
    pub xmin: i32,
    pub ymin: i32,
    pub xmax: i32,
    pub ymax: i32,
}

pub enum ZoneBoundsKind {
    All,
    Generated,
    Inhabited,
}

pub async fn get_zone_bounds(db: &DbPool, kind: ZoneBoundsKind) -> Result<ZoneBounds> {
    let (cx, cy, zoom, table) = match kind {
        ZoneBoundsKind::All => ("cx", "cy", 8u32, "chunks"),
        ZoneBoundsKind::Generated => ("cx", "cy", 8u32, "chunks WHERE data IS NOT NULL"),
        ZoneBoundsKind::Inhabited => ("x", "y", 1u32, "zones"),
    };

    let query = format!(
        r#"
        SELECT
        COALESCE(MIN({0}) * {2}, 0) as xmin,
        COALESCE(MIN({1}) * {2}, 0) as ymin,
        COALESCE((MAX({0}) + 1) * {2}, 0) as xmax,
        COALESCE((MAX({1}) + 1) * {2}, 0) as ymax
        FROM {3};
        "#,
        cx, cy, zoom, table
    );

    Ok(sqlx::query_as(&query).fetch_one(db).await?)
}

pub fn get_inhabited_zones(db: &DbPool) -> impl Stream<Item = Result<(i32, i32)>> + '_ {
    sqlx::query!(
        r#"
        SELECT x, y FROM zones ORDER BY (x, y);
    "#
    )
    .fetch(db)
    .map(|chunk| {
        let c = chunk?;
        Ok((c.x, c.y))
    })
}

pub async fn save_zone(db: &mut DbConn, zone: &DbZone) -> Result<()> {
    let old = if zone.is_empty() {
        // The zone is empty: delete it from the table and
        // return the previous value.
        let deleted = sqlx::query_as!(
            DbZone,
            r#"
            DELETE FROM zones
            WHERE x = $1 AND y = $2
            RETURNING *;
        "#,
            zone.x,
            zone.y
        )
        .fetch_optional(&mut *db)
        .await?;

        if let Some(old) = deleted {
            old
        } else {
            return Ok(()); // The zone doesn't exist, we're done.
        }
    } else {
        loop {
            // Try inserting a new zone; retrieve the current zone if it already exists.
            let is_new = sqlx::query!(r#"
                INSERT INTO zones(x, y, location_end, name, description, owner, owner_id, created_at)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                ON CONFLICT DO NOTHING;
            "#, zone.x, zone.y, zone.location_end, zone.name, zone.description, zone.owner, zone.owner_id,
                zone.created_at).execute(&mut *db).await?.rows_affected() > 0;

            if is_new {
                return Ok(()); // New zone inserted, we're done.
            }

            // Retrieve current zone value and lock the row.
            let old = sqlx::query_as!(
                DbZone,
                r#"
                SELECT * FROM zones
                WHERE x = $1 AND y = $2
                FOR UPDATE;
            "#,
                zone.x,
                zone.y
            )
            .fetch_optional(&mut *db)
            .await?;

            match old {
                Some(zone) => break zone,
                None => continue, // Somebody just deleted the row; retry
            }
        }
    };

    // Check that the new data isn't outdated.
    if zone.created_at < old.created_at {
        return Err(crate::Error::OutdatedData {
            outdated: zone.created_at,
            saved: old.created_at,
        });
    }

    // If the zones are different, update the data
    // and insert the previous value into the history table.
    if !old.compare(zone) {
        sqlx::query!(
            r#"
            UPDATE zones z_cur SET
                location_end = $3,
                name = $4,
                description = $5,
                owner = $6,
                owner_id = $7,
                created_at = $8
            WHERE x = $1 AND y = $2;
        "#,
            zone.x,
            zone.y,
            zone.location_end,
            zone.name,
            zone.description,
            zone.owner,
            zone.owner_id,
            zone.created_at
        )
        .execute(&mut *db)
        .await?;

        sqlx::query!(r#"
            INSERT INTO zones_history(x, y, location_end, name, description, owner, owner_id, valid_range)
            VALUES ($1, $2, $3, $4, $5, $6, $7, TSTZRANGE($8, $9));
        "#, old.x, old.y, old.location_end, old.name, old.description, old.owner, old.owner_id,
            old.created_at, zone.created_at).execute(&mut *db).await?;
    }

    Ok(())
}
