
-- Current chunk data.
CREATE TABLE chunks (
    -- The x chunk coordinate (zone coo. divided by 8).
    cx INTEGER NOT NULL,
    -- The y chunk coordinate (zone coo. divided by 8).
    cy INTEGER NOT NULL,
    -- The chunk kube data, stored in z,y,x order.
    -- NULL if the chunk doesn't exist.
    data BYTEA,
    -- Is the chunk data compressed?
    -- Always TRUE if data is NULL.
    compressed BOOLEAN NOT NULL,
    -- The creation time of this record.
    -- Before this time, the chunk data was different (or unknown).
    created_at TIMESTAMPTZ NOT NULL,
    -- The last time this record was retrieved from the Kube server.
    -- This means that the data didn't change in the range
    -- [created_at, last_checked_at)
    last_checked_at TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (cx, cy),
    CONSTRAINT chk_compressed CHECK (data IS NOT NULL OR compressed),
    CONSTRAINT chk_times CHECK (created_at <= last_checked_at)
);

-- Chunk data history.
CREATE TABLE chunks_history (
    -- See chunks.cx.
    cx INTEGER NOT NULL,
    -- See chunks.cy.
    cy INTEGER NOT NULL,
    -- See chunks.data.
    data BYTEA,
    -- See chunks.compressed.
    compressed BOOLEAN NOT NULL,
    -- If TRUE, the chunk data is stored as a delta: XOR-ed with
    -- the next version of the chunk (no effect if the next version
    -- has no chunk data).
    -- Always FALSE if data is NULL.
    is_delta BOOLEAN NOT NULL,
    -- Start of validity.
    -- The chunk data of this row is valid until the next version.
    valid_since TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (cx, cy, valid_since),
    CONSTRAINT chk_compressed CHECK (data IS NOT NULL OR compressed),
    CONSTRAINT chk_delta CHECK (data IS NOT NULL OR NOT is_delta)
);

-- View unifying `chunks` and `chunks_history`.
CREATE VIEW chunks_full_history AS (
    SELECT * FROM chunks_history UNION ALL
    SELECT
        chunks.cx, chunks.cy,
        chunks.data, chunks.compressed,
        FALSE as is_delta,
        chunks.created_at as valid_since
    FROM chunks
);

-- Metadata about zone locations.
-- There is no last_checked_at field, because zones checks
-- are always made with the corresponding chunk check.
CREATE TABLE zones (
    -- The x zone coordinate.
    x INTEGER NOT NULL,
    -- The y zone coordinate.
    y INTEGER NOT NULL,
    -- The end of the current location (NULL means 
    -- it extends indefinitely in the future).
    location_end TIMESTAMPTZ,
    -- The current owner (NULL means this is a public zone).
    owner VARCHAR(100),
    -- The user_id of the current owner, if it exists.
    owner_id INTEGER,
    -- The name of the zone, if it exists.
    name VARCHAR(100),
    -- The description of the zone, if it exists.
    description TEXT,
    -- The creation time of this record.
    -- Before this time, the zone metadata is unknown.
    created_at TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (x, y)
);

-- Zones metadata history.
CREATE TABLE zones_history (
    -- See zones.x.
    x INTEGER NOT NULL,
    -- See zones.y.
    y INTEGER NOT NULL,
    -- See zones.location_end.
    location_end TIMESTAMPTZ,
    -- See zones.owner.
    owner VARCHAR(100),
    -- See zones.owner_id.
    owner_id INTEGER,
   -- See zones.name.
    name VARCHAR(100),
    -- See zones.description.
    description TEXT,
    -- The range of times over which this record is valid.
    valid_range TSTZRANGE NOT NULL,

    PRIMARY KEY (x, y, valid_range),
    EXCLUDE USING GIST (x WITH =, y WITH =, valid_range WITH &&)
);

-- View unifying `zones` and `zones_history`.
CREATE VIEW zones_full_history AS (
    SELECT * FROM zones_history UNION ALL
    SELECT 
        zones.x, zones.y,
        zones.location_end,
        zones.owner, zones.owner_id,
        zones.name, zones.description,
        TSTZRANGE(zones.created_at, NULL) as valid_range
    FROM zones
);
