# Kube database

Requires PostgreSQL 10 or newer.

## Setup

```sql
-- Create the user and the database
CREATE USER <username> WITH <password>;
CREATE DATABASE <dbname> WITH OWNER <username>;

-- Enable necessary extensions
\c <dbname>
CREATE EXTENSION btree_gist;
```

Finally, put the database URL in an `.env` file, for the `sqlx::query!` macro:
```
DATABASE_URL=postgres://username:password@localhost:5432/dbname
```
